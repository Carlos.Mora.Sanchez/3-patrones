let a, i, aux;
function MostrarP1() {
  a = document.getElementById("n1").value;
  aux = 1;
  for (i = 0; i < a; i++) {
    for (j = 0; j < aux; j++) {
      document.write("*");
    }
    aux++;
    document.write("<br>");
  }
}
function MostrarP2() {
  a = document.getElementById("n1").value;
  for (i = 1; i <= a; i++) {
    aux = "";
    for (k = a - i; k > 0; k--) {
      aux = aux + "&nbsp;&nbsp;";
    }
    for (k = 0; k < i; k++) {
      aux = aux + "*";
    }
    document.write("<br>");
    document.write(aux);
  }
}
function MostrarP3() {
  a = document.getElementById("n1").value;
  for (i = 0; i < a; i++) {
    if (i === 0 || i === a - 1) {
      for (k = 0; k < a; k++) {
        document.write("*");
      }
    } else {
      for (k = 0; k < a; k++) {
        if (k === 0 || k === a - 1) {
          document.write("*");
        } else {
          document.write("&nbsp;&nbsp;");
        }
      }
    }
    document.write("<br>");
  }
}
